const config = {
  pages: 35,
  stage: 2,   //0: main thread scraping, 1: throttled stats scraping, 2: restoring hierarchy
  requestLimit: 300,
  requestObject: {
    withCredentials: true, headers: {
      Cookie: "issession=xxxxxxxxxxxxxxxxxxxxxxxx; iscreds=xxxxxxxxxxxxxxxxxxxxxxxx;"
    }
  },
  threadLink: "https://is.muni.cz/auth/discussion/plkarna/107697007/" //end with slash!
}

const axios = require('axios').default;
const cheerio = require('cheerio');
const moment = require("moment");
const fs = require('fs').promises;
const fsx = require('fs');
let requestsExecuted = 0;
let allocateRequests = (amount) => {
  requestsExecuted += amount;
  if (requestsExecuted > config.requestLimit) throw new Error("Ran out of requests.")
}
(async () => {

  if (config.stage === 0) {
    allocateRequests(config.pages);
    let scraped = await Promise.all(Array(config.pages).fill(0).map(async (x, page) => {

        const {data} = await axios.get(`${config.threadLink}${page + 1}`, config.requestObject);
        const $ = cheerio.load(data);
        return ($('.obsah .text.clearfix')
          .slice(page === 0 ? 0 : 1)     //first entry(root) is present on every page
          .toArray().map(x => {
            let id = $(x).parent().parent().attr('data-id');
            return ({
              id,
              statsLink: `https://is.muni.cz/auth/dok/prehled_cteni?guz=${id}`,
              textContent: $(x).contents().toArray().map(y => $(y).text()
                .replace(/^[\n\t]+/g, "")
                .replace(/[\n\t]+$/g, "")
              )
                .filter(x => x !== "")
                .join("\n")
            });
          }))
      })
    )
    await fs.writeFile('scraped.json', JSON.stringify(scraped.flat().map(x => ({
      object: x,
      processed: false
    })), null, 2));

  } else if (config.stage === 1) {
    if (!(await fsx.existsSync("scrapedWithStats.json"))) await fs.writeFile('scrapedWithStats.json', await fs.readFile("scraped.json", "utf-8"));
    let dataFromScraped = JSON.parse(await fs.readFile('scrapedWithStats.json', 'utf-8'));
    if (dataFromScraped.map(x => x.processed).reduce((a, b) => a && b)) throw new Error("all entries have already been processed with stats scraper.")
    let processedData = dataFromScraped.map(x => {
      let stats;
      return (async () => {
        try {
          if (x.processed === true) return x;
          let current = x.object;
          allocateRequests(1);
          const {data} = await axios.get(current.statsLink, config.requestObject);
          const $ = cheerio.load(data);
          stats = {
            views: $("div.box-pul:nth-child(4) > dl:nth-child(2) > dd:nth-child(2) > b:nth-child(1)").text(),
            viewsByDepartment: Object.fromEntries($("table.data1:nth-child(1) > tbody:nth-child(1) > tr").toArray().slice(1,-1).map(x=>$(x).children().toArray()).map(x=>[$(x[0]).text(),parseInt($(x[1]).text())])),    //if this doesnt work, do NOT debug it(rewrite it from scratch)
            authorUco: $("tr.reset > td:nth-child(1) > dl:nth-child(1) > dd:nth-last-child(1) > a:nth-child(1)").text(),
            authorName: $("tr.reset > td:nth-child(1) > dl:nth-child(1) > dd:nth-last-child(1)").text().split(",")[1],
            creationDate: moment($("tr.reset > td:nth-child(1) > dl:nth-child(1) > dd:nth-last-child(1)").text().split(",")[0], "DD. MM. YYYY HH:mm"),
            reactions: {
              informative: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)").text()),
              offTopic: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2)").text()),
              noOpinion: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2)").text()),
              annoying: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2)").text()),
              amusing: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2)").text()),
              interesting: parseInt($("table.data1:nth-child(2) > tbody:nth-child(1) > tr:nth-child(7) > td:nth-child(2)").text()),
            },

            depth: ($("tr.reset > td:nth-child(1) > dl:nth-child(1) > dd:nth-child(2)").text().match(/ \/ /g) || []).length+1
          }
          return {object: x.object, stats, processed: true};
        } catch (e) {
          return {object: x.object, processed: false}
        }
      })
      ();


    })
    await fs.writeFile("scrapedWithStats.json", JSON.stringify(await Promise.all(processedData), null, 2));
  } else if (config.stage === 2) {
    let dataFromScraped = JSON.parse(await fs.readFile('scrapedWithStats.json', 'utf-8'));
    if (!dataFromScraped.map(x => x.processed).reduce((a, b) => a && b)) throw new Error("not all data has been processed yet")
    let actualDepth = 0;
    let ancestors = [dataFromScraped[0].object.id];
    let processedData = dataFromScraped.map((x, index) => {
      if(index!==0){
        if (x.stats.depth > actualDepth + 1) throw new Error(`na tuto situaciu som nemyslel(depths): prispevok cislo${x.object.id}`)
        actualDepth = x.stats.depth;
        x.parent = ancestors[actualDepth - 1]
        ancestors[actualDepth] = x.object.id
      } else {
        x.parent = -1
      }
      x.stats.reactions.all = Object.values(x.stats.reactions).reduce((a,b) =>a+b)
      return x;
    });
    await fs.writeFile("scrapedWithStatsAndHierarchy.json", JSON.stringify(await Promise.all(processedData), null, 2));
  }
})()