data = require("./scrapedWithStatsAndHierarchy.json");

console.log(data.length + " odpovedi");

console.log(data.map(x => x.object.textContent).join("").length + " znakov")
console.log(data.map(x => x.object.textContent).join(" ").split(" ").length + " slov")
console.log(data.map(x => x.stats.reactions.all).reduce((a, b) => a + b) + " reakcii")


let makeLink = (id) => `https://is.muni.cz/auth/discussion/plkarna/107697007/-/${id}`


flatData = data.map(x => ({
  views: x.stats.views,
  id: x.object.id,
  author: x.stats.authorName,
  allReactions: x.stats.reactions.all,
  annoyingReactions: x.stats.reactions.annoying,
  amusingReactions: x.stats.reactions.amusing,
  informativeReactions: x.stats.reactions.informative,
  interestingReactions: x.stats.reactions.interesting,
  offTopicReactions: x.stats.reactions.offTopic,
  noOpinionReactions: x.stats.reactions.noOpinion,
  text: x.object.textContent,
  depth: x.stats.depth,
  parent: x.parent
}))
console.log("\nNajviac zanorene prispevky")
console.table(flatData
  .sort((a,b) => b.depth-a.depth)
  .map(({author,depth,text,id})=>({author,depth,link:makeLink(id),text:text.slice(0,60)}))
  .slice(0,16))

console.log("\nNajviac zanorene prispevky")
console.table(flatData

  .map(({
           views,
           id,
           author,
           allReactions,
           annoyingReactions,
           amusingReactions,
           informativeReactions,
           interestingReactions,
           offTopicReactions,
           noOpinionReactions,
           text,
         }) => ({
  noOpinionReactions,
  offTopicReactions,
  interestingReactions,
  views,
  id,
  author,
  link: makeLink(id),
  text: text.slice(0, 60)

}))
  .sort((a, b) => -a.interestingReactions + b.interestingReactions)
  .slice(0, 10))