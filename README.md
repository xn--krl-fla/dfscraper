# Discussion Forum scraper
This application is for scraping discussion forums from Information System of Masaryk's university in Czech Republic. (viewable after logging in to [is.muni.cz](https://is.muni.cz)).
The discussion forum threads are probably only visible after logging in. If you are not able to log in, the script will not work.

## Installation and configuration
This script needs Node.js, probably at least 8.0 (maybe more, maybe less, it worked on my machine lol). Dockerizing it was not worth it yet.
You will need internet browser with cookie editing support. My suggestion is Firefox + EditCookie plugin.

run 
```shell script
npm install
```

then go to your index.js and edit following things in config variable:

* pages - number of pages of thread you want to scrape
* requestObject.withCredentials.Cookie - your auth cookies from IS
    * Go to IS in your browser.
    * open cookie editor and copy both cookies. 
    * Use following syntax to fill the value in config: `"issession=xxxxxxxxxxxxxxxxxxxxxxxx; iscreds=xxxxxxxxxxxxxxxxxxxxxxxx;"`
* threadLink - link to a thread you want to scrape

## Running

### First stage
the first stage is to **scrape the pages of thread**. Change `config.stage` in `index.js` to `0` and run
```shell script
npm start
```
The script should create a file named `scraped.json`. It contains entries in the thread in the following form:
```javascript
[
  {
    "object": {
      "id": "107697007",
      "statsLink": "https://is.muni.cz/auth/dok/prehled_cteni?guz=107697007",
      "textContent": "yeeeeeeeeeeeeeeet"
    },
    "processed": false
  },
//...other objects...
]
```
### Second stage

the second stage is **scraping every entry in thread for its statistics**. 
Change `config.stage` in `index.js` to `1` and run
```shell script
npm start
```
This will copy the `scraped.json` to `scrapedWithStats.json` and process `x` entries (`x` being your `config.requestLimit`).
You will need to repeat this step until all objects aren't processed. 
The stage 2 will throw error if this has happened, so you don't need to check it manually.
`scrapedWithStats.json` will contain entries in the following form:
```javascript
[
  {
      "object": {
        "id": "107697007",
        "statsLink": "https://is.muni.cz/auth/dok/prehled_cteni?guz=107697007",
        "textContent": "yeeeeeeeeeeeeeeet"
      },
      "stats": {
        "views": "4096",
        "viewsByDepartment": {
          "$id $department": 1024,
        },
        "authorUco": "65536",
        "authorName": "yeetus",
        "creationDate": "2020-11-11T11:11:00.000Z",
        "reactions": {
          "informative": "1",
          "offTopic": "2",
          "noOpinion": "3",
          "annoying": "4",
          "amusing": "5",
          "interesting": "6"
        },
        "depth": 1
      },
      "processed": true
    },
//...other objects...
]
```
#### Warning and complications
This queries IS for every entry in wanted thread, so it can and probably will trigger antispam protection! 
You can use https://is.muni.cz/auth/system/antiscraping to reset the counters or wait.
At the time of writing, the protection was triggered after 1000 requests - that equals ~3 second stage runs with default settings (`config.requestLimit=300`)

### Third stage
The third stage is **rebuilding inferred hierarchy from reported depths of entries**.

Change `config.stage` in `index.js` to `2` and run
```shell script
npm start
```

This stage doesn't use webscraping, it just processes data to infer more data.
Currently it only adds entry.parent(inferred from changes of depth of entries) and sum of reactions.
The file `scrapedWithStatsAndHierarchy.json` should be created by this stage, containing parent id for every entry.

## Disclaimer

This code is not supposed to be pretty or reusable.
You are responsible for the life of the data you scrape and generate. 

## Thanks

My thanks goes to all developers that built tools that are used in this project.

